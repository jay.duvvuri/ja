import './common.cmd'
import cyTerminalReportInstallLogsCollector from 'cypress-terminal-report/src/installLogsCollector'
cyTerminalReportInstallLogsCollector()
require('cypress-xpath')
require('cypress-get-it')
Cypress.Screenshot.defaults({
	screenshotOnRunFailure: true,
})
