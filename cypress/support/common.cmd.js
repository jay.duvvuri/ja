Cypress.Commands.add('login', function (employeeId) {
	var testUserId = 'test@test.com'
	var permissions = {
		userId: testUserId,
		name: testUserId,
		email: testUserId,
		isAdmin: { value: true },
		isEmployee: employeeId
			? {
					value: true,
					employeeId: employeeId,
			  }
			: { value: false },
		isPlanner: { value: true, allowedPlanningUnits: { type: 'all' } },
	}

	var permissionsSigningKey = Cypress.env('permissions_signing_key')

	cy.task('jwtSign', {
		payload: permissions,
		secret: permissionsSigningKey,
		options: { expiresIn: '24h' },
	}).then(function (permissionsJwt) {
		if (!permissionsJwt) {
			cy.log('No permissions cookie generated!')
		} else {
			cy.setCookie('permissions', encodeURIComponent(permissionsJwt))
		}
	})
})

Cypress.Commands.add('openAppAdminUI', () => {
	return cy.visit('/admmin')
})

Cypress.Commands.add('isVisible', selector => {
	return cy.get(selector).should('be.visible')
})

Cypress.Commands.add('isDisabled', selector => {
	return cy.get(selector).should('be.disabled')
})

Cypress.Commands.add('isEnabled', selector => {
	return cy.getDataTestID(selector).should('not.be.disabled')
})

Cypress.Commands.add('isNotVisible', selector => {
	return cy.get(selector).should('not.visible')
})

Cypress.Commands.add('isButtonEnabled', selector => {
	return cy.get(selector).should('not.be.disabled')
})

Cypress.Commands.add('isButtonDisabled', selector => {
	return cy.get(selector).should('be.disabled')
})

Cypress.Commands.add('isNotExist', selector => {
	return cy.get(selector).should('not.exist')
})

Cypress.Commands.add('isExist', selector => {
	return cy.get(selector).should('exist')
})

Cypress.Commands.add('isTextVisible', text => {
	return cy.contains(text, { timeout: 10000 }).should('be.visible')
})

Cypress.Commands.add('textClick', text => {
	return cy.contains(text, { timeout: 10000 }).click()
})

Cypress.Commands.add('clickSelector', selector => {
	return cy.get(selector).click()
})

Cypress.Commands.add('verifyTestFlagEnabled', () => {
	return cy
		.get('body')
		.invoke('data', 'feature-flags')
		.should('contain', 'cypress-test-flag-enabled')
})

Cypress.Commands.add('gotoUrl', text => {
	return cy.visit(text)
})
