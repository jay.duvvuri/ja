// ### 1. Launch the Application
// ### 2. Goto Marketplace and check Test Employee is visible and click it
// ### 3. Get the Employee ID and PIN
// ### 4. Login to the App with Employee ID
// ### 5. Shift should have been optimized by LTP
// ### 6. Create a shift for employee on Long term planning
// ### 7. Shift should have been optimized by intradayplanning
// ### 8. The Test task is published on the planner UI automated test market under the shifts section
// ### 9. Login to the Employee UI TA-PAD
// ###10. Verify the message - "you have 1 task today"
// ###11. PunchIn and Punch Out with given the PIN code for the respective Employee

import {
	marketPlace,
	marketPlaceWorkload,
	employeeUrl,
	testEmployee,
	inactive,
	dataEmployeePin,
	planningPeriodTxt,
	calculateTxt,
	workloadUpdatedTxt,
	testTaskTxt,
	taskTxtMsgToday,
	shifts,
	automatedTestMarket,
	punchedInSuccessfully,
	containsCancelFilter,
	punchedOutSuccessfully,
} from '../../fixtures/testData.json'
import {
	employeePin,
	shiftsLoadingIndicator,
	draft,
	shiftAtFour,
	saveButton,
	shiftEditor,
	ltpButton,
	dayOptButton,
	optimizeLtp,
	optDayDraftsOnly,
	taskType,
	shiftAtEight,
	btnPublished,
	shiftSaveButton,
	taskSegment,
	dashboardTodaysTask,
	plannedShiftTitle,
	confirmPunch,
	punchOut,
} from '../../selectors/locators'
import dayjs from 'dayjs'

let emPIN, empID
const today = dayjs().format('YYYY-MM-DD')

before(() => {
	cy.login()
	cy.gotoUrl(marketPlace)
	cy.isTextVisible(testEmployee)
	cy.contains(testEmployee).click({ force: true })

	cy.contains(inactive).should(label => {
		expect(
			label,
			'Test employee is inactive, cannot run the tests.' +
				' Please activate the Test Employee user from the database' +
				' (set inactive field false in the employees table)'
		).not.exist
	})

	cy.url().then($currentUrl => {
		var tmpID = $currentUrl.split('/')
		empID = parseInt(tmpID[tmpID.length - 1])
		cy.log('Got employee id ' + empID)
	})

	cy.isVisible(employeePin)
	cy.get(employeePin).then($element => {
		const pin = $element.attr(dataEmployeePin)
		if (!pin)
			throw new Error('Could not find [data-employee-pin] from the DOM!')
		emPIN = pin
		cy.log('Got employee PIN: ' + emPIN)
	})
})

const ShiftStateString = [
	'undefined',
	'long_term_planning',
	'tasks_generated',
	'planning',
	'published',
	'confirmed',
]

describe('E2E Test - Publish and View shifts ', () => {
	it('Publish Employee shift for today, check in employee UI and punch in/out', () => {
		cy.log('Logging in with employee ID ' + empID)
		cy.login(empID)

		cy.gotoUrl(marketPlaceWorkload)
		cy.isTextVisible(planningPeriodTxt)
		cy.textClick(planningPeriodTxt)
		cy.isTextVisible(calculateTxt)
		cy.textClick(calculateTxt)
		cy.isTextVisible(workloadUpdatedTxt)
		cy.isTextVisible(testTaskTxt)

		// Go to shifts
		cy.isTextVisible(shifts)
		cy.textClick(shifts)

		// wait until shifts are loaded
		cy.get(shiftsLoadingIndicator, { timeout: 10000 }).should('not.exist')

		// open modal to clear the tasks
		cy.log('Shift was already generated, initializing to draft shift')
		cy.get(`[data-cy=shift-gantt-${empID}-${today}]`).should('be.visible')
		cy.get(`[data-cy=shift-gantt-${empID}-${today}]`).click()

		// set as draft, with 4:00 hours
		cy.isVisible(draft)
		cy.clickSelector(draft)

		cy.isVisible(shiftAtFour)
		cy.clickSelector(shiftAtFour)

		cy.isButtonEnabled(saveButton)
		cy.clickSelector(saveButton)

		cy.isNotExist(shiftEditor)
		cy.get(`[data-cy=shift-gantt-${empID}-${today}]`).should('contain', '4')

		// optimize LTP
		cy.clickSelector(ltpButton)
		cy.clickSelector(optimizeLtp)

		cy.log('Shift should have been optimized by LTP')
		cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-long_term_planning']`,
			{
				timeout: 10000,
			}
		).should('exist')
		cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-long_term_planning']`,
			{
				timeout: 10000,
			}
		).should('be.visible')

		// optimize intra-day

		cy.clickSelector(dayOptButton)
		cy.clickSelector(optDayDraftsOnly)

		cy.log('Shift should have been optimized by intradayplanning')
		cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-tasks_generated']`,
			{
				timeout: 10000,
			}
		).should('exist')

		cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-tasks_generated']`,
			{
				timeout: 10000,
			}
		).should('be.visible')

		// cy.get(`shift-gantt-${empID}-${today}`, { timeout: 10000 }).click()
		cy.xpath(`//div[@data-cy='shift-gantt-${empID}-${today}']`).click()

		cy.log(
			'The expected test task should have been assigned by intradayplanning'
		)
		cy.isExist(taskType)

		// save as 8 hours shift & publish
		cy.clickSelector(shiftAtEight)
		cy.clickSelector(btnPublished)
		cy.clickSelector(shiftSaveButton)

		cy.isNotExist(shiftEditor)

		const publishContainer = cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-published']`,
			{
				timeout: 10000,
			}
		)

		publishContainer.should('contain', '8')

		cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-published']`,
			{
				timeout: 10000,
			}
		).should('exist')
		cy.xpath(
			`//div[@data-cy='shift-gantt-${empID}-${today}']//div[@data-cy='shift-state-published']`,
			{
				timeout: 10000,
			}
		).should('be.visible')

		// Check employee UI
		cy.gotoUrl(employeeUrl)
		cy.get(dashboardTodaysTask).should('contain', taskTxtMsgToday)

		cy.isTextVisible(shifts)
		cy.textClick(shifts)

		cy.isExist(plannedShiftTitle)

		// Check TA-pad
		cy.visit('/ta', {
			auth: {
				username: Cypress.env('ta_pad_basic_username'),
				password: Cypress.env('ta_pad_basic_password'),
			},
		})

		cy.contains(automatedTestMarket).click()

		var loginWithPin = function (pinNumbers) {
			pinNumbers.forEach(function (pinNumber) {
				cy.log('pin Number:' + pinNumber)
				return cy.get(`[data-testid="${pinNumber}"]`).click()
			})
		}
		let loginPIN = emPIN.split('')

		loginWithPin(loginPIN)
		cy.get(taskSegment).should('exist')

		cy.get('button').then(function ($buttons) {
			const $punchInBtn = $buttons.filter('[data-testid="punchIn"]')
			if ($punchInBtn.length > 0) {
				// not yet punched in
				$punchInBtn.click()
				cy.get(confirmPunch).click()
				cy.contains(punchedInSuccessfully)
			} else {
				cy.log(
					'Already punched in, e.g due to previous execution being interrupted. Cancel and punch out later.'
				)
				var $cancelBtn = $buttons.filter(containsCancelFilter)
				$cancelBtn.click()
			}
		})
		loginWithPin(loginPIN)
		cy.get(punchOut).click()
		cy.get(confirmPunch).click()
		cy.isTextVisible(punchedOutSuccessfully)
	})
})
