// ### 1. Launch the Application
// ### 2. Goto Marketplace and check Test Employee is visible and click it
// ### 3. Get the Employee ID from the current url
// ### 4. Login to the App with Employee ID
// ### 5. Goto 'manage' and check whether the test flag is enabled
// ### 6. Goto 'employees' and check whether the test flag is enabled
// ### 7. Goto 'admin' and check whether the test flag is enabled

import {
	marketPlace,
	testEmployee,
	manageUrl,
	employeeUrl,
	adminUrl,
} from '../../fixtures/testData.json'

let empID

before(() => {
	cy.login()
	cy.gotoUrl(marketPlace)
	cy.isTextVisible(testEmployee)
	cy.contains(testEmployee).click()

	cy.url().then($currentUrl => {
		var tmpID = $currentUrl.split('/')
		empID = parseInt(tmpID[tmpID.length - 1])
	})
})

describe('Feature Flags', () => {
	it('Check feature flags present in the DOM', () => {
		cy.login(empID)
		cy.gotoUrl(manageUrl)
		cy.verifyTestFlagEnabled()

		cy.gotoUrl(employeeUrl)
		cy.verifyTestFlagEnabled()

		cy.gotoUrl(adminUrl)
		cy.verifyTestFlagEnabled()
	})
})
