const jwt = require('jsonwebtoken')
const cyTerminalReportInstallLogsPrinter = require('cypress-terminal-report/src/installLogsPrinter')

module.exports = (on, config) => {
	on('task', {
		jwtSign({ payload, secret, options }) {
			return jwt.sign(payload, secret, options)
		},
	})

	cyTerminalReportInstallLogsPrinter(on)
}
